﻿var config = require('./config.global');

config.env = "test";
config.db.type = "SQLite";
config.db.name = "sql_test_db";

module.exports = config;