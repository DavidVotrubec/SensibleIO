﻿//Based on config variable it should select db instance
var env = process.env.NODE_ENV || 'development';
var config = require("../config." + env);
var Mongorito = require('mongorito');
var Model = Mongorito.Model;
var sqlite = require("sqlite3");
var constants = require("./constants");
var blogPost = require("./schema/BlogPostSchema");

var scriptName = "DbWrapper";

var DbWrapper = function DbWrapperFn() {

	Mongorito.connect(config.db_connection, config.db.name);

	function *find(where) {

		if (config.db.type == constants.DB_TYPES.MongoDb) {
			var posts = yield blogPost.model.find(where);
			return posts;
		}

		//TODO: branch for SQLite
	}

	//TODO: Avoid callback hell by using generators *
	function *save(blogPostObj) {

		if (config.db.type == constants.DB_TYPES.MongoDb) {
			var post = new blogPost.model(blogPostObj);
			yield post.save();
		}

		//TODO: branch for SQLite

	}
	
	//TODO: add also delete 
	return {
		find: find,
		save: save
	}
}();

module.exports = DbWrapper;
