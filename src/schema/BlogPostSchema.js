﻿var Mongorito = require('mongorito');
var Model = Mongorito.Model;

//TODO: blogPostSchemaRaw should be possible to reuse in SQLite .. perhaps
var blogPostSchemaRaw = {
	Id: Number,
	Title: String,
	Text: String,
	Created_at: { type: Date, default: Date.now }
};

class BlogPostModel extends Model {

}

module.exports = {
	schema: blogPostSchemaRaw,
	model: BlogPostModel
};