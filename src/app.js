﻿var env = process.env.NODE_ENV || 'development';
var config = require('../config.' + env); //load config depending on NODE_ENV variable
var koa = require('koa');
var json = require('koa-json');
var logger = require('koa-logger');
var route = require('koa-route');
var dbWrapper = require('./DbWrapper__OLD');
var app = koa();

//prety json response by default, see https://www.npmjs.com/package/koa-json
app.use(json());

// Based on example https://github.com/koajs/examples/blob/master/blog/index.js

// route middleware
app.use(route.get('/', list));
app.use(route.get('/list', list));
app.use(route.get('/:id', show));


//app.use(route.get('/post/new', add));
//app.use(route.get('/post/:id', show));
//app.use(route.post('/post', create));
//app.use(route.delete('/post/:id', remove));

function* list(next) {
    var posts = dbWrapper.find(); 
    
	this.body = posts;
}

function *show(id) {
    this.body = "id is: " + id;
}

//Add error logging
app.on('error', function (err) {
    log.error('server error', err);
});

app.listen(config.port);