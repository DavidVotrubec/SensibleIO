﻿var constants = {};

constants.DB_TYPES = {};
constants.DB_TYPES.MongoDb = "MongoDb";
constants.DB_TYPES.SQLite = "SQLite";


//usage in windows cmd: set NODE_ENV=test
constants.ENV_TYPES = {};
constants.ENV_TYPES.Test = "test";
constants.ENV_TYPES.Development = "development";
constants.ENV_TYPES.Production = "production";

module.exports = constants;