module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: ['dist'],

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        qunit: {
            files: ['test/**/*.html']
        },
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint', 'qunit']
        },

        babel: {
            options: {
                sourceMap: true,
                modules: "common", //was "common"
                optional: ['runtime']     //Otherwise error "regeneratorRuntime is not defined" is thrown
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src',
                    src: ['**/*.js'],
                    dest: 'dist/src',
                    ext:'.js'
                }]
            },
            distSpecs: {
                files: [{
                    expand: true,
                    cwd: 'spec',
                    src: ['**/*.js'],
                    dest: 'dist/spec',
                    ext:'.js'
                }]
            }
        },

        /*babel: {
            options: {
                sourceMap: false,
                modules: "common", //was "common"
                optional: ['runtime']     //Otherwise error "regeneratorRuntime is not defined" is thrown
            },
            dist: {
                files: {
                    'dist/app.js': 'dist/app.concat.js'
                }
            }
        } ,*/

        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['dist/src/**/*.js'],
                //dest: 'dist/<%= pkg.name %>.js'
                dest: 'dist/app.concat.js'
            }
        },

        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: ['config.*.js'],
                        dest: 'dist/',
                        filter: 'isFile'},
                ],
            },
        },

        mochacli: {
            options: {
                harmony: true,
                require: ['should'],
                files: 'test/*.js'
            },
            all: ['test/*js']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-nodeunit');
    grunt.loadNpmTasks('grunt-mocha-cli');
    grunt.loadNpmTasks('grunt-babel');

    grunt.registerTask('test', ['clean', 'jshint', 'qunit']);

    //grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);
    grunt.registerTask('default', ['clean', 'babel', 'copy']);

};