
require("babel/register")({
    optional: ['runtime']
});

var gulp = require('gulp');                  // must stay
var babel = require('gulp-babel'); //must stay

require("babel/register")({
    optional: ['runtime', 'es7.asyncFunctions']
});

var browserify = require('browserify');
var babelify= require('babelify');
var gutil = require('gulp-util');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var es = require('event-stream');
var concat = require('gulp-concat');



gulp.task('default', function () {
    return gulp.src('src/**/*.js')
        .pipe(babel())
        .pipe(gulp.dest('lib'));
});


gulp.task('sourcemaps', function () {
    return gulp.src('src/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat('all.js'))  //TODO: Ok?????
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('lib'));
});