﻿//this is the default config file

var exports = module.exports = {};
var config = {};

config.env = "development";
config.hostname = "dev.example.com";

//database type
config.db = {};
//options are [MongoDB, SQLite]
config.db.type = "MongoDB"; //default value
config.port = process.env.WEB_PORT || 3000;
config.db_connection = "mongodb://localhost:27017/";

module.exports = exports = config;