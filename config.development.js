﻿var config = require('./config.global');

config.env = "development";
config.db.type = "MongoDb";
config.db.name = 'mongo_development_db';

module.exports = config;