﻿# SensibleIO Test Project
## Because it sounds like a cool place to work at
Task description: https://gist.github.com/cyner/00ea9ef4c4939e34b746

Url of application is localhost:3000

## What I needed to read/study in order to get started
### Readings about Koa.JS
- http://code.tutsplus.com/tutorials/introduction-to-generators-koajs-part-1--cms-21615

### About SQLite in nodejs
- http://blog.modulus.io/nodejs-and-sqlite

## About ES6
- Using ES6 today http://mammal.io/articles/using-es6-today/
- Babel transpiler https://babeljs.io/

### Managing app configuration
- http://stackoverflow.com/questions/5869216/how-to-store-node-js-deployment-settings-configuration-files
- http://www.chovy.com/node-js/managing-config-variables-inside-a-node-js-application/

### Unit Testing

run command "grunt mochacli"

- http://www.chovy.com/node-js/testing-a-node-js-express-app-with-mocha-and-should-js/
- nice article: http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
- *Dynamically generating tests*: https://mochajs.org/#getting-started
- testing mongoose http://www.scotchmedia.com/tutorials/express/authentication/1/06
- Mongorito seems like a better choice than mongoose since it is based on ES6 generators, just like Koa http://mongorito.com/

# Prerequisities
- MongoDB has to be installed on windows machine - http://blog.gvm-it.eu/post/20462477195/getting-started-with-mongodb-and-nodejs-on
- installing just node driver for mongodb is not enough
- after installation you need to update PATH variable - add c:\Program Files\MongoDB\Server\3.0\bin\
- then start mongoDB server and point it to your data folder where it will store its database in a file
mongod -dbpath "path_to_project\data"
- That will manully start the DB. Needs to be done before the app starts (any workaround or automation for this??)


## TODO:
- Write DbWrapper to access/store data in MongoDb/SQLite depending on app configuration
- Add unit tests for DbWrapper:
-- find a way how to perform the SAME unit tests on both MongoDb and SQLite databases
-- the app should behave the same way, so the tests should be the same as well
- Router for different endpoints
- Create test coverage report http://brianstoner.com/blog/testing-in-nodejs-with-mocha/

## TODO 2:
- when all is done and I have time then add Instanbul for Code Coverage http://www.clock.co.uk/blog/tools-for-unit-testing-and-quality-assurance-in-node-js
- and  try to play with JsHInt etc


I am trying to use generator* but it does not work. The asterix is not valid syntax and transforming ES6 to ES5 does not work either.
Babel does not complile correctly, I can not run generated script because of some obscure "regeneratorRuntime is not defined".
It is flustrating ... none of  the tutorials actually works ... too many options and none of thee compilers is emitting runnable code ... Grrrrrr

I will try Grunt instead of Gulp ... see if that helps to compile ES6
Setup grunt like this http://rockyj.in/2015/05/24/es6_with_babel_grunt.html