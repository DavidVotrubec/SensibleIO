﻿var constants = require("../dist/src/constants");
var blogPost = require('../dist/src/schema/BlogPostSchema');
var DbWrapper = require('../dist/src/DbWrapper');
var should = require('should');
var co = require('co');

//use co-mocha for testing generators.
//dont forget to add .then(done, done) otherwise tests will timeout
var mocha = require('mocha');
var coMocha = require('co-mocha');

//monkey patch mocha with support for generators
coMocha(mocha);


beforeEach(function(){
    //TODO: Clear DB
    console.log("clear db");
});

//run the same tests for both Test and Development settings
//so that both mongoDB and SQLite parts are tested
//var environments = [constants.ENV_TYPES.Test, constants.ENV_TYPES.Development];
var environments = [constants.ENV_TYPES.Development];
environments.forEach(function (env) {

    process.env.NODE_ENV = env;
    var config = require('../config.' + env);

    //Sanity check
    if (process.env.NODE_ENV === constants.ENV_TYPES.Production) {
        console.log("NEVER RUN TEST ON PRODUCTION DB !!!!");
        process.exit(1);
    }

    describe("BlogPosts API", function () {

        it("should store new BlogPost", function (done) {
            //TODO: typescript interfaces would be nice here

            co(function *() {
                var blogPostObj = {
                    Title: "Test blog post 1",
                    Text: "Text of the post"
                };

                yield DbWrapper.save(blogPostObj);

                //TODO: retrieve just saved post from db
                var posts = yield DbWrapper.find(blogPostObj);
                console.log('stored posts', posts.length);
                posts.length.should.equal(1);
            }).then(done, done);

        });
    });

});
