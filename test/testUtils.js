﻿var env = process.env.NODE_ENV || 'development';
var config = require("../config." + env);
var Mongorito = require('mongorito');
var Model = Mongorito.Model;
//tODO: Test also agains sqlite3
var sqlite = require("sqlite3");
var co = require("co");
var constants = require("../src/constants");
var blogPost = require("../dist/src/schema/BlogPostSchema");

function clearDb(done) {
    Mongorito.connect(config.db_connection, config.db.name);

    co(function *() {
       yield blogPost.model.remove();
    }).then(done, done);
}

//run this automatically before each test 
//because command "npm test" runs all files in the "test" directory
beforeEach(function(done) {
    clearDb(done);
});

//automatically disconnect after each test
afterEach(function (done) {
    Mongorito.disconnect();
	return done();
});

//I do not need to export it, it is run automatically by "npm test" command
//module.exports = {
//    beforeEach: beforeEach,
//    afterEach: afterEach
//}